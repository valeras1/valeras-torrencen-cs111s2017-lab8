//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Samantha Valera and Natasha Torrence 
// CMPSC 111 Spring 2017
// Lab #8
// Date: March 9 2017 
//
// Purpose: Suduko Puzzle
//***********************
import java.util.Date; 
import java.util.Scanner;

public class SudokuCheckerMain
{
	public static void main(String[] args)
	{
		 

		// This is our output
		System.out.println("Samantha Valera and Natasha Torrence\nLab #8\n"); 
		System.out.println(new Date() + "\n");

		SudokuChecker checker = new SudokuChecker();
		
		
		System.out.print("Welcome to Sudoku Checker: to check your Sudoku, enter your board one row at a time, with each digit separated by a space.  Hit ENTER at the end of a row. ");
		
		//This calls the inputGridRow method from Sudoku Checker	
		checker.inputGridRow(); 
		//This calls the checkGrid method from Sudoku Checker
		checker.checkGrid();
		
		
		
	}
}
